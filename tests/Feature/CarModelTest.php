<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class CarModelTest extends TestCase
{
    use DatabaseMigrations;

    private $token;
    private $brand;
    private $carModel;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $user = User::create([
            'name' => 'test',
            'email' => 'test@email.com',
            'paswword' => 12345678
        ]);

        $brand = Brand::create([
            'brand_name' => 'test',
            'user_id' => $user->_id
        ]);

        $this->brand = $brand;
        $this->token = JWTAuth::fromUser($user);

//        dd($this->brand->_id);
        $this->get_models();
        $this->create_models();
        $this->update_models();
        $this->delete_models();
    }

    public function get_models()
    {
        $response = $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('get', '/api/brand/'.$this->brand->_id.'/carModel');

        $response->assertStatus(200);
    }

    public function create_models()
    {
        $response = $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('post', '/api/brand/'.$this->brand->_id.'/carModel',[
                'model_name' => 'php_unit_test_name'
            ]);

        $this->carModel = json_decode($response->getContent())->data[0];

        $response->assertStatus(200);
    }

    public function update_models()
    {
        $response = $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('put', '/api/brand/'.$this->brand->_id.'/carModel/'.$this->carModel->id,[
                'model_name' => 'php_unit_test_name_new'
            ]);

        $response->assertStatus(200);
    }

    public function delete_models()
    {
        $response = $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('delete', '/api/brand/'.$this->brand->_id.'/carModel/'.$this->carModel->id);

        $response->assertStatus(200);
    }
}
