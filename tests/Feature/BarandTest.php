<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tymon\JWTAuth\Facades\JWTAuth;

class BarandTest extends TestCase
{

    use DatabaseMigrations;

    private $token;
    private $brand;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $user = User::create([
            'name' => 'test',
            'email' => 'test@email.com',
            'paswword' => 12345678
        ]);

        $this->token = JWTAuth::fromUser($user);

        $this->get_brands();
        $this->create_brands();
        $this->get_my_brands();
        $this->update_brands();
    }

    public function get_brands()
    {
        $response = $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('get', '/api/brand');

        $response->assertStatus(200);
    }

    public function create_brands()
    {
        $response = $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('post', '/api/brand', [
                'brand_name' => 'php_unit_brand_test'
            ]);

        $response->assertStatus(200);
    }

    public function get_my_brands()
    {
        $response = $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('get', '/api/user/brands');

        $this->brand = json_decode($response->getContent())->data[0];
        $response->assertStatus(200);
    }

    public function find_brands()
    {
        $response = $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('post', 'find/brand',[
                'brand_name' => 'php_unit_brand_test'
            ]);

        $response->assertStatus(200);
    }

    public function update_brands()
    {
        $response = $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('put', '/api/brand/'.$this->brand->id, [
                'brand_name' => 'php_unit_brand_test_new'
            ]);

        $response->assertStatus(200);
    }

    public function delete_brands()
    {
        $response = $this->withHeader('Authorization', 'Bearer ' . $this->token)
            ->json('delete', '/api/brand/'.$this->brand->id);

        $response->assertStatus(200);
    }
}
