<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends TestCase
{

    use DatabaseMigrations;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->register_test();
        $this->login_test();
    }

    public function register_test()
    {
        $response = $this->post('/api/auth/singup', [
            'name' => 'Test',
            'email' => 'test1@hotmail.com',
            'password' => '12345678'
        ]);
        //$response = $this->post('/');

        $response->assertStatus(201);
    }

    public function login_test()
    {
        $response = $this->post('/api/auth/login', [
            'email' => 'test1@hotmail.com',
            'password' => '12345678'
        ]);
        //$response = $this->post('/');

        $response->assertStatus(200);
    }


}
