<?php

namespace App\Services;

use App\Http\Requests\Api\FindBrandRequest;
use App\Http\Resources\BrandResources;
use App\Models\CarModel;

class Brand
{
    public function findBrand($request)
    {
        if(isset($request->brand_name)){
            $brands = \App\Models\Brand::where('brand_name', 'LIKE', '%'.$request->brand_name.'%')->paginate(20);
        } else if(isset($request->model_name)){
            $models = CarModel::where('model_name', $request->model_name)->first();
            $brands = $models->brand();
        }

        return new BrandResources($brands->paginate(20));
    }
}
