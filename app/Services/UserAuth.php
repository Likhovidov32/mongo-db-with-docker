<?php

namespace App\Services;

use App\Http\Resources\UserTokenResource;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use JWTAuth;

class UserAuth
{
    public function loginOrFail($request)
    {
        $user = User::where('email', $request->email)->first();

        if(!$user || !Hash::check($request->password, $user->password)){

            return response()->json([
                'success' => false,
                'message' => "Incorrect email or password."
            ], 403);
        }

        return self::createTokenResponse($user);
    }

    public function createUser($request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);


        return self::createTokenResponse($user);
    }

    private static function createTokenResponse($user)
    {
        if(!$user->token = JWTAuth::fromUser($user)) {
            return response()->json([
                'success' => false,
                'message' => "Can`t create token."
            ], 403);
        }

        return new UserTokenResource($user);
    }
}
