<?php


namespace App\Facedes;

use Illuminate\Support\Facades\Facade;

class UserAuth extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'userauth';
    }
}
