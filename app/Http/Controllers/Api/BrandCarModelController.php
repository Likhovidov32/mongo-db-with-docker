<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CreateBrandCarModelRequest;
use App\Http\Requests\Api\UpdateBrandCarModelRequest;
use App\Http\Resources\CarModelResources;
use App\Models\Brand;
use App\Models\CarModel;
use Illuminate\Http\Request;

class BrandCarModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return CarModelResources
     */
    public function index()
    {
        return new CarModelResources(CarModel::paginate(20));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return CarModelResources
     */
    public function store(CreateBrandCarModelRequest $request,Brand $brand)
    {
        $brand->carModels()->create($request->validated());

        return new CarModelResources(CarModel::paginate(20));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CarModel  $carModel
     * @return CarModelResources
     */
    public function update(UpdateBrandCarModelRequest $request, Brand $brand, CarModel $carModel)
    {
        $carModel->model_name = $request->model_name;
        $carModel->save();

        return new CarModelResources(CarModel::paginate(20));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CarModel  $carModel
     * @return CarModelResources
     */
    public function destroy(Brand $brand, CarModel $carModel)
    {
        $carModel->delete();

        return new CarModelResources(CarModel::paginate(20));
    }
}
