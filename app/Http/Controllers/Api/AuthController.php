<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\SignUpRequest;
use App\Http\Resources\UserTokenResource;
use JWTAuth;
use App\Facedes\UserAuth;

class AuthController extends Controller
{
    public function signup(SignUpRequest $request)
    {
        return UserAuth::createUser($request);
    }

    public function login(LoginRequest $request)
    {
        return UserAuth::loginOrFail($request);
    }
}
