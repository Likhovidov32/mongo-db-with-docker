<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateBrandRequest;
use App\Http\Requests\Api\DeleteBrandRequest;
use App\Http\Requests\Api\FindBrandRequest;
use App\Http\Requests\Api\UpdateBrandRequest;
use App\Http\Resources\BrandResources;
use App\Models\Brand;
use App\Models\CarModel;
use App\Http\Controllers\Controller;
use App\Facedes\Brand as BrandService;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return BrandResources
     */
    public function index()
    {
        return new BrandResources(Brand::paginate(20));
    }

    public function usersBrands()
    {
        return new BrandResources(Brand::where('user_id', auth()->user()->_id)->paginate(20));
    }

    public function findBrand(FindBrandRequest $request)
    {
        return BrandService::findBrand($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return BrandResources
     */
    public function store(CreateBrandRequest $request)
    {
        auth()->user()->brands()->create($request->validated());

        return new BrandResources(Brand::paginate(20));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return BrandResources
     */
    public function update(UpdateBrandRequest $request, Brand $brand)
    {
        $brand->brand_name = $request->brand_name;
        $brand->save();

        return new BrandResources(Brand::paginate(20));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return BrandResources
     */
    public function destroy(DeleteBrandRequest $request, Brand $brand)
    {
        $brand->delete();

        return new BrandResources(Brand::paginate(20));
    }
}
