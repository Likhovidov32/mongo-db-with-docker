<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\ApiFormRequest;

class UpdateBrandCarModelRequest extends ApiFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'model_name' => [
                "required",
                "unique:car_models,model_name"
            ]
        ];
    }
}
