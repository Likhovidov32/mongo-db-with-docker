<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Brand extends Eloquent
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $fillable = [
        'brand_name', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function carModels()
    {
        return $this->hasMany(CarModel::class)->orderBy('id', 'DESC');
    }
}
