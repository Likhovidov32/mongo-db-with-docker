<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class CarModel extends Eloquent
{
    use HasFactory;

    protected $connection = 'mongodb';

    protected $fillable = [
        'brand_id', 'model_name'
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }
}
