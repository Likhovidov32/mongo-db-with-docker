<?php

use App\Http\Controllers\Api\BrandCarModelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BrandController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'], function () {

    Route::post('singup', [AuthController::class, 'signup'])->name('api.auth.singup');
    Route::post('login', [AuthController::class, 'login'])->name('api.auth.login');
});

Route::group(['middleware' => ['jwt.verify']], function () {

    Route::apiResource('brand', BrandController::class);
    Route::get('user/brands', [BrandController::class, 'usersBrands']);
    Route::post('find/brand', [BrandController::class, 'findBrand']);

    Route::apiResource('brand.carModel', BrandCarModelController::class);
});
